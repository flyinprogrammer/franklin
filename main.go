package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/ec2metadata"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	retry "github.com/giantswarm/retry-go"
	log "github.com/sirupsen/logrus"
)

const (
	tagName = "env"
	tmpFile = "/tmp/instance-data.env"
)

type Info struct {
	AvailabilityZone string `env:"AVAILABILITY_ZONE"`
	DatacenterID     string `env:"DATACENTER_ID"`
	Domain           string `env:"DOMAIN"`
	Env              string `env:"ENV"`
	FQDN             string `env:"FQDN"`
	Hostname         string `env:"HOSTNAME"`
	InstanceID       string `env:"INSTANCE_ID"`
	Mac              string `env:"MAC"`
	Name             string `env:"NAME"`
	PrivateIPAddr    string `env:"PRIVATE_IP_ADDR"`
	Region           string `env:"REGION"`
	VPCID            string `env:"VPC_ID"`
}

func (inf *Info) ToEnvFile(filePath string) {
	file, err := os.Create(filePath)
	if err != nil {
		log.WithError(err).Fatal("Cannot create file")
	}
	defer file.Close()

	v := reflect.ValueOf(*inf)
	for i := 0; i < v.NumField(); i++ {
		// Get the field tag value
		tag := v.Type().Field(i).Tag.Get(tagName)
		// Skip if tag is not defined or ignored
		if tag == "" || tag == "-" {
			continue
		}
		structValue := v.Field(i).Interface()
		fmt.Fprintf(file, "export %s=\"%s\"\n", tag, structValue.(string))
	}
}

type ErrNoTags struct {
	message string
}

func NewErrNoTags(message string) *ErrNoTags {
	return &ErrNoTags{
		message: message,
	}
}

func (e *ErrNoTags) Error() string {
	return e.message
}

func IsErrNoTags(err error) bool {
	_, ok := err.(*ErrNoTags)
	return ok
}

type ErrTooManyTags struct {
	message string
	count   int
}

func NewErrTooManyTags(message string, count int) *ErrTooManyTags {
	return &ErrTooManyTags{
		message: message,
		count:   count,
	}
}

func (e *ErrTooManyTags) Error() string {
	return e.message
}

func main() {
	blammoPtr := flag.Bool("blammo", false, "force the refresh of the data")
	flag.Parse()

	_, err := os.Stat(tmpFile)
	fileNotExists := os.IsNotExist(err)

	var cached bool

	if *blammoPtr || fileNotExists {
		i := GenerateAWSInfo()
		i.ToEnvFile(tmpFile)
	} else {
		cached = true
	}
	file, err := os.Open(tmpFile)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
	fmt.Println("# Blammo: " + strconv.FormatBool(*blammoPtr))
	fmt.Println("# File Not Exists: " + strconv.FormatBool(fileNotExists))
	fmt.Println("# Using cached: " + strconv.FormatBool(cached))
}

func GenerateAWSInfo() Info {
	sess := session.Must(session.NewSession())
	emSVC := ec2metadata.New(sess)
	doc, err := emSVC.GetInstanceIdentityDocument()
	if err != nil {
		panic(err)
	}

	mac, err := emSVC.GetMetadata("/mac")
	if err != nil {
		log.WithError(err).Fatal("error getting mac address")
	}

	dcID, err := emSVC.GetMetadata("/network/interfaces/macs/" + mac + "/vpc-id")
	if err != nil {
		log.WithError(err).Fatal("error getting datacenter id")
	}

	eSVC := ec2.New(sess, &aws.Config{Region: aws.String(doc.Region)})

	var env string
	fetchEnv := func() error {
		env, err = GetTag(eSVC, doc.InstanceID, "Env")
		if err != nil {
			return err
		}
		return nil
	}

	err = retry.Do(fetchEnv, retry.RetryChecker(IsErrNoTags), retry.Sleep(10*time.Second), retry.MaxTries(20), retry.Timeout(5*time.Minute))
	if err != nil {
		log.WithError(err).Fatal("error after trying to fetch Env tag an inappropriate amount of times")
	}

	var name string
	fetchName := func() error {
		name, err = GetTag(eSVC, doc.InstanceID, "Name")
		if err != nil {
			return err
		}
		return nil
	}
	err = retry.Do(fetchName, retry.RetryChecker(IsErrNoTags), retry.Sleep(10*time.Second), retry.MaxTries(20), retry.Timeout(5*time.Minute))
	if err != nil {
		log.WithError(err).Fatal("error after trying to fetch Name tag an inappropriate amount of times")
	}

	hostname := ExecHostname("-s")
	fqdn := ExecHostname("-f")
	domain := ExecHostname("-d")

	return Info{
		AvailabilityZone: doc.AvailabilityZone,
		DatacenterID:     dcID,
		Domain:           domain,
		Env:              env,
		FQDN:             fqdn,
		Hostname:         hostname,
		InstanceID:       doc.InstanceID,
		Mac:              mac,
		Name:             name,
		PrivateIPAddr:    doc.PrivateIP,
		Region:           doc.Region,
		VPCID:            dcID,
	}

}
func ExecHostname(flag string) string {
	cmdOut, err := exec.Command("hostname", flag).Output()
	if err != nil {
		log.WithError(err).Fatal("error using hostname cmd with flag: " + flag)
	}

	return strings.TrimSuffix(string(cmdOut), "\n")
}

func GetTag(eSVC *ec2.EC2, instanceID string, name string) (string, error) {
	tags, err := eSVC.DescribeTags(&ec2.DescribeTagsInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name: aws.String("resource-id"),
				Values: []*string{
					aws.String(instanceID),
				},
			},
			&ec2.Filter{
				Name: aws.String("key"),
				Values: []*string{
					aws.String(name),
				},
			},
		},
	})
	if err != nil {
		return "", err
	}

	tagCount := len(tags.Tags)

	if tagCount == 0 {
		log.Warn("asked for " + name + " but it came back with 0 tags")
		return "", NewErrNoTags(fmt.Sprintf("no tags found for %s", name))
	}
	if tagCount > 1 {
		log.Warn("asked for " + name + " but it came back with " + strconv.Itoa(tagCount) + " many tags, expected 1")
		return "", NewErrTooManyTags(fmt.Sprintf("too many (%d) tags found for %s", tagCount, name), tagCount)
	}

	return *tags.Tags[0].Value, nil
}
